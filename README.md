# Summary of Semester 6

2018 © Jinsun Lim

## Basic

## Why is it a good idea to use something like TypeScript or Babel?

<p align="center">

<img src ="https://cdn-images-1.medium.com/max/1600/1*8lKzkDJVWuVbqumysxMRYw.png">

</p>

- [Why you should use typeCsript](https://stackoverflow.com/questions/12694530/what-is-typescript-and-why-would-i-use-it-in-place-of-javascript/35048303#35048303)


## Why is const better than let and var?

This is why I favor `const` over `let` in ES6. In JavaScript, `const` means that the identifier can’t be reassigned. (Not to be confused with immutable values. Unlike true immutable datatypes such as those produced by Immutable.js and Mori, a `const` object can have properties mutated.)
If I don’t need to reassign, `const` is my default choice over `let` because I want the usage to be as clear as possible in the code.

I use `let` when I need to reassign a variable. Because I use one variable to represent one thing, the use case for `let` tends to be for loops or mathematical algorithms.

I don’t use `var` in ES6. There is value in block scope for loops, but I can’t think of a situation where I’d prefer `var` over `let`.

> var

var and variables in es5 has scopes in functions meaning the variables are valid within the function and not outside the function itself. The code above hence outputs same results as the previous one. With this information what is the output of the next code?:

````
function printing(){
  for(var i = 0; i<10; i++) {
    console.log(i)
  }
}
printing()
console.log(i)
````
Last line of code will retrun "undefinded". as var is only accecable in the function scope.

> let

whereas let, you can access it from outside of fuction.
````
for(let i = 0; i<10; i++) {
  console.log(i)
}
console.log(i)
````

> const

Const actually means that once the variable is assigned it cannot be assigned again and an attempt to do so will throw an error.
but not to be confused immutable and mutable. const variable can be mutable.
````
const dog={
  age: 3
}
dog.age = 5 // it's ok to change state of object inside.
dog = { name: 'biko'} // error - not allowed assign.
````


- [Referrence](https://medium.com/javascript-scene/javascript-es6-var-let-or-const-ba58b8dcde75)
- [Referrence2](https://medium.com/craft-academy/javascript-variables-should-you-use-let-var-or-const-394f7645c88f)


## What is hoisting?

Hoisting is you can "declare" variable "later". 


````
x = 5; // Assign 5 to x

consolo.log(x);                // Display x

var x; // Declare x
````

not to be confused with "Initialize"

````
var x = 5; // Initialize x

console.log(x);
console.log(y);       // Display x and y. y is undefinded.

y = 7;     // Assign 7 to y
var y;     // Declare y
````


Hoisting is (to many developers) an unknown or overlooked behavior of JavaScript.
If a developer doesn't understand hoisting, programs may contain bugs (errors).
To avoid bugs, always declare all variables at the beginning of every scope.
Since this is how JavaScript interprets the code, it is always a good rule.

JavaScript in strict mode does not allow variables to be used if they are not declared.


## Function as first-class citizen


In JavaScript, functions are objects (hence the designation of first-class object). They inherit from the Object prototype and they can be assigned key: value pairs. These pairs are referred to as properties and can themselves be functions (i.e., methods). And as mentioned, function objects can be assigned to variables, they can be passed around as arguments; they can even be assigned as the return values of other functions. Demonstrably, functions in JavaScript are first-class objects.

> Can we assign a function to a variable?

````

var func = function (param) {
  return param;
}

document.body.innerHTML += func("Did I just call a variable? Yep!");

````

> Can we pass a function as an argument to another function?

````

var func = function (param) {
  return param;
}

function func1 (param1) {
  return param1;
}

//We'll inject return values in to p elements.. just for readability
var pElems = document.querySelectorAll("p");
//..................................................................

pElems[0].innerHTML += func(func1);

//We can even pass a function WITH an argument, AS an argument!
pElems[1].innerHTML += func(func1("arg1"));

````

> But can we return a function… from a function?? (Hint: We already did, but… let’s see it again!)

````

function returnAFunc () {
  return function returnedFunc (param) {
    return param;
  }
}

var pElems = document.querySelectorAll("p");

//return the inner function from the outer function
pElems[0].innerHTML += returnAFunc();

//call the returned function, with an argument... return the argument!
pElems[1].innerHTML += returnAFunc()("Hi! ");

````


